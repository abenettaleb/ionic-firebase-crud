// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyBH3rnNPxI5oO244Gy7XWdRV3lIVH8tqis',
    authDomain: 'ionic-firebase-crud-fea3b.firebaseapp.com',
    databaseURL: 'https://ionic-firebase-crud-fea3b.firebaseio.com',
    projectId: 'ionic-firebase-crud-fea3b',
    storageBucket: 'ionic-firebase-crud-fea3b.appspot.com',
    messagingSenderId: '713078550797',
    appId: '1:713078550797:web:f032bdc35d12eaa23d0044',
    measurementId: 'G-1PF5YBFKFD',
  },
}

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
